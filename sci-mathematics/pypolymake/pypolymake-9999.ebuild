# Distributed under the terms of the GNU General Public License v2

EAPI=3

inherit mercurial distutils

DESCRIPTION="Python interface for polymake"
HOMEPAGE="http://bitbucket.org/burcin/pypolymake"
EHG_REPO_URI="http://bitbucket.org/burcin/pypolymake"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="sci-mathematics/sage[testsuite]
	sci-mathematics/polymake[libpolymake]"
RDEPEND="${DEPEND}"
