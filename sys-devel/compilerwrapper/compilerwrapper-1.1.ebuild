# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
inherit eutils

DESCRIPTION=""
HOMEPAGE="https://bitbucket.org/vbraun/compilerwrapper"
# The repository doesn't have any tags
# this should correspond to the contents of 1.0 spkg
SRC_URI="http://bitbucket.org/vbraun/compilerwrapper/get/compilerwrapper-1.1.tar.bz2"
S="${WORKDIR}/vbraun-compilerwrapper-faa09082bce1"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64-linux x86-linux"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_configure() {
	# guess the prefix and postfix
	CC_CMD=`tc-getCC`
	CC_PRE=${CC_CMD%gcc*}
	CC_POST=${CC_CMD#*gcc}
	einfo "Setting compiler prefix: $CC_PRE, postfix: $CC_POST"
	econf --with-ccpath=/usr/bin --with-ldpath=/usr/bin \
		--program-prefix=$CC_PRE --program-suffix=$CC_POST\
		--with-gcc-transform-name="s/^/$CC_PRE/"
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
	dodoc AUTHORS NEWS README TODO
}
